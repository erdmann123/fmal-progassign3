% Forritunarverkefni III
% FMAL 2014
%
% Emil Holm
% emilh11@ru.is
%
% Hjalti Erdmann
% hjaltis11@ru.is

% 1. ---------
%
% a)
%

married(tom,jane).
married(jane,tom).

havingAffair(steve,jane).
havingAffair(steve,mary).
havingAffair(mary,steve).
havingAffair(jane,steve).

rich(steve).

greedy(john).

hatred(HATER,HATEE) :- married(HATER,ADULTRESS), havingAffair(HATEE,ADULTRESS).

greed(SUSPECT,VICTIM) :- greedy(SUSPECT), rich(VICTIM), not(rich(SUSPECT)).

motive(SUSPECT,VICTIM) :- hatred(SUSPECT,VICTIM).
motive(SUSPECT,VICTIM) :- greed(SUSPECT,VICTIM).

% b) 

% motive(X,steve).

%	X = tom
%	X = john

%
% c)
%

% rich(john). %% missing fact



% 2. --------
%
% a)
%

numElements(0,[]).
numElements(X,[_|T]) :- numElements(X1,T), X is X1+1.

%
% b)
%
% Mooshak -> hjatis11, 749, 212:30:54;

removeElement(X, [X|T], T).
removeElement(X, [H|T], [H|L]) :- removeElement(X, T, L).

%
% c)
% Mooshak -> emilh11, 446,  191:27:01

insertElementAt(X,L,1,[X|L]).
insertElementAt(X,[Y|L1],P,[Y|L2]) :- P > 1,P1 is P-1,insertElementAt(X,L1,P1,L2).

%
% d)
%
% Mooshak -> hjatis11, 848, 213:59:54;
%
checkSegment([],_).
checkSegment([H|T1], [H|T2]) :- !,checkSegment(T1,T2).

segment([],_).
segment([H|T1], [H|T2]) :- checkSegment(T1,T2).
segment([H|T1], [A|T2]) :- not(H=A), segment([H|T1],T2). 

%
% e)
%
% Mooshak - emilh11 - 828 213:40:38

addUpListHelper([],[Acc|_],Acc).
addUpListHelper([Y|L1],[Acc|L2],Acc) :- Acc1 is Y+Acc, addUpListHelper(L1,L2,Acc1).
addUpListHelper([Y|L1],L2,0) :- addUpListHelper(L1,L2,Y).
addUpList([Y|L1],L2) :- addUpListHelper(L1,L2,Y).
addUpList([],_).

%
% 3. --------------------
%
% a) mymerge
%
% Mooshak -> emilh11, 991, 217:13:16

mymerge([],[],[]).
mymerge(L,[],L).
mymerge([],L,L).
mymerge([X|L1],[Y|L2],[Y|L3]) :- X > Y,!, mymerge([X|L1],L2,L3).
mymerge([X|L1],[Y|L2],[X|L3]) :- X =< Y,!, mymerge(L1,[Y|L2],L3).

%
% b) mysplit
%
% Mooshak -> emilh11, 973, 216:10:42

mysplit([],[],[]).
mysplit([X],[],[X]).
mysplit([X,Y|L1],[X|L2],[Y|L3]) :- mysplit(L1,L2,L3).

%
% c) mysort
%
% Mooshak -> emilh11, 996, 217:32:59

mysort([],[]).
mysort([X],[X]).
mysort(L1,X) :- mysplit(L1,L2,L3), mysort(L2,Y),mysort(L3,Z),mymerge(Y,Z,X). 


% 4. --------------------
%
% a)
%
% Mooshak -> hjaltis11, 903, 214:47:03

binaryTree(nil).
binaryTree(t(_,nil,nil)).
binaryTree(t(_,B,nil)) :- binaryTree(B).
binaryTree(t(_,nil,C)) :- binaryTree(C).
binaryTree(t(_,B,C)) :- binaryTree(B), binaryTree(C).


%
% b)
%
% Moosah -> hjaltis11, 929, 215:37:56

preorder(nil, []).
preorder(t(A,nil,nil),[A]).
preorder(t(A,B,nil),[A|L]) :- preorder(B,L).
preorder(t(A,nil,C),[A|L]) :- preorder(C,L).
preorder(t(A,B,C),[A|L])   :- preorder(B,L1),preorder(C,L2), append(L1,L2,L). 


%
% c)
%
% Moosah -> hjaltis11, 978, 216:15:44

leaves(nil, []).
leaves(t(A,nil,nil), [A]).
leaves(t(_,B,nil), L) :- leaves(B,L).
leaves(t(_,nil,C), L) :- leaves(C,L).
leaves(t(_,B,C), L)   :- leaves(B,L1), leaves(C,L2), append(L1,L2,L).


%
% 5. -------------------
%
% Moosah -> hjaltis11, 1449, 215:37:56
%

expr(L) :- term(L).
expr(L) :- append(T1, L2, L),append(L1,[+],T1),term(L1), expr(L2).

term(L) :- factor(L).
term(L) :- append(T1, L2, L), append(L1, [*], T1),factor(L1), term(L2).

factor([H|T]) :- number(H), T == [].
factor([H|T]) :- H == '(', append(X, [')'], T), expr(X).
