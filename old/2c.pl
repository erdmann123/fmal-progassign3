append([],L,L).
append([X|L1],L2,[X|L3]) :- append(L1,L2,L3).
% Append fall fengid héðan:
% http://cs.union.edu/~striegnk/learn-prolog-now/html/node47.html#subsec.l6.defining.append


insertElementAt(X,[],0,[X|L]).
insertElementAt(X,L1,1,[X|L2]).
insertElementAt(X,[Y|L1],P,[Y|L2]) :- P>1,P1 is P-1,insertElementAt(X,L1,P1,[Y|L2]).


%insertElementAt(X,L1,1,L2) :- insertElementAt(X,L1,0,[X|L2]).


%insertElementAt(c,[a,b,d],3,[]). -> (c,[b,d],2,[a])
%insertElementAt(c,[b,d],2,[a]). -> (c,[d],1,[a,b])
%insertElementAt(c,[d],1,[a,b]). -> (c,[d],0,[a,b,c])
%insertElementAt(c,[],0,[a,b,c]). -> (c,[],0,[a,b,c,d])
